#include <cxxopts/cxxopts.hpp>
#include <external_sort/external_sort.h>

#include <filesystem>
#include <chrono>
#include <fstream>

int main(int argc, char** argv) {
    try {
        cxxopts::Options options("sort_file", "Sort file");

        std::size_t min_buffer = 3 * sizeof(std::int64_t);

        options.add_options()
                ("s,size", "buffer size in bytes, min " + std::to_string(min_buffer), cxxopts::value<std::uint64_t>())
                ("i,input", "input name file", cxxopts::value<std::string>())
                ("o,output", "output name file", cxxopts::value<std::string>());

        auto result = options.parse(argc, argv);

        if (result.count("help") || result.count("size") == 0 ||
            (result.count("size") && result["size"].as<std::uint64_t>() < min_buffer) || result.count("input") == 0) {
            std::cout << options.help() << std::endl;
            return 0;
        }


        auto original_file_name = result["input"].as<std::string>();

        auto output_file_name = [&] {
            if (result.count("output") == 0) {
                return original_file_name + ".sorted";
            }
            return result["output"].as<std::string>();
        }();

        std::cout << "input file: " << original_file_name << std::endl;
        std::cout << "ouptut file: " << output_file_name << std::endl;

        if (std::filesystem::exists(output_file_name)) {
            std::cout << "Warning: output file is exist" << std::endl;
            return 0;
        }

        auto file_size = std::filesystem::file_size(original_file_name);
        if (file_size % sizeof(std::int64_t) != 0) {
            std::cout << "Error: file has the wrong size" << std::endl;
            return 0;
        }

        std::cout << "size file: " << file_size << std::endl;
        std::size_t count_item = file_size / sizeof(std::int64_t);
        std::cout << "count item: " << count_item << std::endl;
        std::size_t buffer_size = result["size"].as<std::uint64_t>() / sizeof(std::int64_t);
        std::cout << "buffer size: " << buffer_size << std::endl;

        auto temp_dir = std::filesystem::temp_directory_path() /
                        ("sort_temp_" + std::to_string(std::chrono::system_clock::now().time_since_epoch().count()));
        std::cout << "temp dir: " << temp_dir << std::endl;
        std::filesystem::create_directories(temp_dir);

        auto final_stage_filename = original_file_name + ".stage_final";

        external_sort::algorithm(original_file_name,
                                 buffer_size,
                                 [&](const std::string& filename, std::size_t offset, char* buffer,
                                     std::size_t count) {
                                     // Read
                                     try {
                                         auto path = filename == original_file_name ? std::filesystem::path(
                                                 original_file_name) : temp_dir / filename;
                                         std::ifstream file(path, std::ios::binary | std::ios::in);
                                         file.seekg(offset * sizeof(std::int64_t));
                                         file.read(reinterpret_cast<char*>(&buffer[0]), count * sizeof(std::int64_t));
                                         file.close();
                                     } catch (const std::exception& e) {
                                         std::cout << "Read error: " << e.what() << std::endl;
                                         throw std::runtime_error("Read failed.");
                                     }
                                 },
                                 [&](const std::string& filename, const char* buffer, std::size_t count) {
                                     // Write
                                     try {
                                         auto path = filename == final_stage_filename ?
                                                     std::filesystem::path(output_file_name) : temp_dir / filename;
                                         std::ofstream file(path, std::ios::binary | std::ios::app);
                                         file.write(reinterpret_cast<const char*>(&buffer[0]),
                                                    count * sizeof(std::int64_t));
                                         file.close();
                                     } catch (const std::exception& e) {
                                         std::cout << "Write error: " << e.what() << std::endl;
                                         throw std::runtime_error("Write failed.");
                                     }
                                 },
                                 [&](const std::string& filename) -> std::size_t {
                                     // Size
                                     auto path = filename == original_file_name ?
                                                 std::filesystem::path(original_file_name) :
                                                 temp_dir / filename;
                                     try {
                                         return std::filesystem::file_size(path) / sizeof(std::int64_t);
                                     } catch (const std::exception& e) {
                                         std::cout << "Write error: " << e.what() << std::endl;
                                         throw std::runtime_error("Get size failed.");
                                     }
                                 });
        std::cout << "sort finished." << std::endl;

    } catch (const std::exception& e) {
        std::cout << "Error: " << e.what() << std::endl;
    }

    return 0;
}
