//
// Created by mtsvetkov on 25.05.2020.
//

#pragma once

#include <vector>
#include <functional>
#include <string>

namespace external_sort {

struct ReadChunkResult {
    bool is_end_file = false;
    bool is_read_chunk_end = false;
};

// read_file(filename, offset, char* buffer, size)
// write_file(filename, const char* buffer, size)
// size_t size_file(filename)
template<typename ReadFile, typename WriteFile, typename SizeFile>
void algorithm(std::string name_file, std::size_t buffer_size, ReadFile&& read_file,
                             WriteFile&& write_file, SizeFile&& size_file) {

    if (buffer_size % 2 == 1) {
        buffer_size += 1;
    }

    std::size_t global_chunk_size = buffer_size / 2;

    std::size_t original_file_size = std::invoke(size_file, name_file);

    {
        std::size_t step = buffer_size;
        std::size_t current_position_file = 0;

        std::vector <std::int64_t> c_buffer(buffer_size);


        while (current_position_file < original_file_size) {
            std::size_t end_position =
                    (current_position_file + step) > original_file_size ? original_file_size : current_position_file +
                                                                                               step;

            // Пишем в наш буффер
            auto c_size_buffer = end_position - current_position_file;
            c_buffer.resize(c_size_buffer);
            std::invoke(read_file,
                        name_file,
                        current_position_file,
                        reinterpret_cast<char*>(&c_buffer[0]),
                        c_buffer.size());

            auto median_it = [&] {
                if (c_buffer.size() <= global_chunk_size) {
                    return c_buffer.end();
                }
                return c_buffer.begin() + global_chunk_size;
            }();

            std::sort(c_buffer.begin(), median_it == c_buffer.end() ? median_it : median_it + 1);
            std::size_t position_median = std::distance(c_buffer.begin(), median_it);
            std::invoke(write_file,
                        "output_file_1.stage_1",
                        reinterpret_cast<const char*>(&c_buffer[0]),
                        position_median);

            if (median_it != c_buffer.end()) {
                std::sort(median_it, c_buffer.end());
                std::invoke(write_file,
                            "output_file_2.stage_1",
                            reinterpret_cast<const char*>(&c_buffer[position_median]),
                            std::distance(median_it, c_buffer.end()));
            }

            current_position_file = end_position;
        }
    }

    std::size_t output_buffer_size = buffer_size / 2;

    if (output_buffer_size % 2 == 1) {
        output_buffer_size -= 1;
        buffer_size -= 1;
    }

    std::size_t input_buffer_size = output_buffer_size / 2;

    auto read_from_file = [&](std::vector <std::int64_t>& buffer, std::string filename, std::size_t chunk_size) {
        std::size_t file_size = size_file(filename);
        return [filename,
                file_size,
                input_buffer_size,
                chunk_size, current_position_file = std::size_t(0), need_read_from_chunck = chunk_size,
                &buffer,
                &read_file]()mutable {

            ReadChunkResult result;
            result.is_read_chunk_end = input_buffer_size > need_read_from_chunck;
            std::size_t new_buffer_size = result.is_read_chunk_end ? need_read_from_chunck : input_buffer_size;

            if (result.is_read_chunk_end) {
                need_read_from_chunck = chunk_size;
            } else {
                need_read_from_chunck -= input_buffer_size;
            }

            result.is_end_file = (current_position_file + new_buffer_size) > file_size;
            if (result.is_end_file) {
                result.is_read_chunk_end = true;
                new_buffer_size = file_size - current_position_file;
            }

            buffer.resize(new_buffer_size);

            std::invoke(read_file,
                        filename,
                        current_position_file,
                        reinterpret_cast<char*>(&buffer[0]),
                        buffer.size());

            current_position_file += buffer.size();

            return result;
        };
    };

    std::vector <std::int64_t> left_buffer(input_buffer_size);
    std::vector <std::int64_t> right_buffer(input_buffer_size);
    std::vector <std::int64_t> output_buffer(output_buffer_size);

    bool is_canceled = false;

    std::size_t stage = 1;

    while (!is_canceled) {

        auto left_filename = "output_file_1.stage_" + std::to_string(stage);
        auto right_filename = "output_file_2.stage_" + std::to_string(stage);

        auto read_file_1 = read_from_file(left_buffer,
                                          left_filename,
                                          global_chunk_size);
        auto read_file_2 = read_from_file(right_buffer,
                                          right_filename,
                                          global_chunk_size);

        global_chunk_size *= 2;

        if (global_chunk_size >= original_file_size) {
            is_canceled = true;
        }

        ReadChunkResult read_file_1_result;
        ReadChunkResult read_file_2_result;

        std::size_t output_file_number = 1;
        ++stage;

        auto write_output_buffer = [&](std::vector <std::int64_t>& buffer,
                                       std::size_t offset) {
            auto output_buffer_name = is_canceled ? name_file + ".stage_final" : "output_file_" +
                                                                                 std::to_string(output_file_number) +
                                                                                 ".stage_" + std::to_string(stage);
            std::invoke(write_file,
                        output_buffer_name,
                        reinterpret_cast<const char*>(&buffer[offset]),
                        buffer.size() - offset);
        };

        while (!read_file_1_result.is_end_file || !read_file_2_result.is_end_file) {
            std::size_t left_pos = 0;
            std::size_t right_pos = 0;
            std::size_t output_pos = 0;

            if (!read_file_1_result.is_end_file) {
                read_file_1_result = ReadChunkResult();
            }

            if (!read_file_2_result.is_end_file) {
                read_file_2_result = ReadChunkResult();
            }

            bool first_pass = true;

            while (!read_file_1_result.is_read_chunk_end || !read_file_2_result.is_read_chunk_end) {
                if ((!read_file_1_result.is_read_chunk_end && left_pos == left_buffer.size()) || first_pass) {
                    read_file_1_result = read_file_1();
                    left_pos = 0;
                }

                if ((!read_file_2_result.is_read_chunk_end && right_pos == right_buffer.size()) || first_pass) {
                    read_file_2_result = read_file_2();
                    right_pos = 0;
                }

                first_pass = false;

                while (left_pos < left_buffer.size() && right_pos < right_buffer.size()) {
                    if (left_buffer[left_pos] > right_buffer[right_pos]) {
                        output_buffer[output_pos] = right_buffer[right_pos];
                        ++right_pos;
                    } else {
                        output_buffer[output_pos] = left_buffer[left_pos];
                        ++left_pos;
                    }
                    ++output_pos;
                    if (output_pos == output_buffer.size()) {
                        write_output_buffer(output_buffer, 0);
                        output_pos = 0;
                    }
                }

                if ((read_file_1_result.is_read_chunk_end && left_pos == left_buffer.size()) ||
                    (read_file_2_result.is_read_chunk_end && right_pos == right_buffer.size())) {
                    if (output_pos != 0) {
                        auto old_size = output_buffer.size();
                        output_buffer.resize(output_pos);
                        write_output_buffer(output_buffer, 0);
                        output_buffer.resize(old_size);
                        output_pos = 0;
                    }
                    if (left_pos != left_buffer.size()) {
                        write_output_buffer(left_buffer, left_pos);
                    }
                    if (right_pos != right_buffer.size()) {
                        write_output_buffer(right_buffer, right_pos);
                    }
                    while (!read_file_1_result.is_read_chunk_end) {
                        read_file_1_result = read_file_1();
                        write_output_buffer(left_buffer, 0);
                    }
                    while (!read_file_2_result.is_read_chunk_end) {
                        read_file_2_result = read_file_2();
                        write_output_buffer(right_buffer, 0);
                    }
                    left_pos = 0;
                    right_pos = 0;
                }
            }
            output_file_number = output_file_number == 1 ? 2 : 1;
        }

        output_file_number = 1;

    }
}
}