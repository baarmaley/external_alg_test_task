cmake_minimum_required(VERSION 3.16 FATAL_ERROR)

project(catch VERSION 2.12.1 LANGUAGES CXX)

add_library(catch_lib INTERFACE)
add_library(catch::catch ALIAS catch_lib)

target_include_directories(catch_lib INTERFACE ${PROJECT_SOURCE_DIR}/include)
target_sources(catch_lib INTERFACE
    ${PROJECT_SOURCE_DIR}/include/catch/catch.hpp
    )
