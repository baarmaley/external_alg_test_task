#include <cxxopts/cxxopts.hpp>

#include <iostream>
#include <filesystem>
#include <fstream>

int main(int argc, char** argv) {
    try {
        cxxopts::Options options("check_file", "Check sorted file");

        options.add_options()
                ("i,input", "checked file", cxxopts::value<std::string>());

        auto result = options.parse(argc, argv);

        if (result.count("help") || result.count("input") == 0) {
            std::cout << options.help() << std::endl;
            return 0;
        }


        auto name_file = result["input"].as<std::string>();
        auto file_size = std::filesystem::file_size(name_file);
        if (file_size % sizeof(std::int64_t) != 0) {
            std::cout << "Error: file has the wrong size" << std::endl;
            return 0;
        }
        std::cout << "size file: " << file_size << std::endl;
        std::size_t buffer_size = file_size / sizeof(std::int64_t);
        std::cout << "count item: " << buffer_size << std::endl;
        std::vector<std::int64_t> buffer;
        buffer.resize(buffer_size);
        std::ifstream file(name_file, std::ios::binary | std::ios::in);
        file.read(reinterpret_cast<char*>(&buffer[0]), file_size);
        file.close();
        std::cout << "is file sorted: " << (std::is_sorted(buffer.begin(), buffer.end()) ? "true" : "false")
                  << std::endl;
    } catch (const std::exception& e) {
        std::cout << "Error: " << e.what() << std::endl;
    }
    return 0;
}
