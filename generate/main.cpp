#include <cxxopts/cxxopts.hpp>

#include <iostream>
#include <fstream>

#include <random>
#include <algorithm>

int main(int argc, char** argv) {
    try {
        cxxopts::Options options("generate_unsorted_files", "Generate unsorted file");

        options.add_options()
                ("s,size", "size in KiB", cxxopts::value<std::uint64_t>())
                ("o,output",
                 "generate name file",
                 cxxopts::value<std::string>()->default_value("original_files.unsorted"));

        auto result = options.parse(argc, argv);

        if (result.count("help") || result.count("size") == 0) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        auto size_file = result["size"].as<std::uint64_t>();
        auto name_file = result["output"].as<std::string>();
        std::size_t count_element = (size_file * 1024) / sizeof(std::int64_t);

        std::cout << count_element << " elements will be generated" << std::endl;
        std::cout << "filename: " << name_file << std::endl;

        auto random_number = [] {
            using limit_64 = std::numeric_limits<std::int64_t>;
            auto randomFunc = [distribution_ = std::uniform_int_distribution<std::int64_t>(limit_64::min(),
                                                                                           limit_64::max()),
                    random_engine_ = std::mt19937{std::random_device{}()}]() mutable {
                return distribution_(random_engine_);
            };
            return randomFunc;
        };

        std::vector<std::int64_t> r_vector;
        r_vector.reserve(count_element);
        std::generate_n(std::back_inserter(r_vector), count_element, random_number());

        std::ofstream file(name_file, std::ios::binary | std::ios::trunc);
        file.write(reinterpret_cast<const char*>(&r_vector[0]), r_vector.size() * sizeof(std::int64_t));
        file.close();

        std::cout << "file generation completed" << std::endl;
    } catch (const std::exception& e) {
        std::cout << "Error: " << e.what() << std::endl;
    }

    return 0;
}
