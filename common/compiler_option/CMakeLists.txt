cmake_minimum_required(VERSION 3.16 FATAL_ERROR)

add_library(CommonCompilerOption INTERFACE)
add_library(common::compiler_option ALIAS CommonCompilerOption)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        target_compile_options(CommonCompilerOption INTERFACE -Wall -Wextra)
    endif()
    if(CMAKE_GENERATOR STREQUAL "Ninja")
        target_compile_options(CommonCompilerOption INTERFACE -fdiagnostics-color=always)
    endif()
endif()
