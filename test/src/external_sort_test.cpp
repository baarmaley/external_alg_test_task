﻿#include "catch/catch.hpp"

#include <external_sort/external_sort.h>

#include <cstring>


TEST_CASE("external algorithm test") {
    auto make_test_env = [](std::size_t original_size, std::size_t buffer_size) {
        auto random_number = [] {
            using limit_64 = std::numeric_limits<std::int64_t>;
            auto randomFunc = [distribution_ = std::uniform_int_distribution<std::int64_t>(0,
                                                                                           100),
                    random_engine_ = std::mt19937{std::random_device{}()}]() mutable {
                return distribution_(random_engine_);
            };
            return randomFunc;
        };

        std::vector<std::int64_t> r_vector;
        r_vector.reserve(original_size);
        std::generate_n(std::back_inserter(r_vector), original_size, random_number());

        std::unordered_map<std::string, std::vector<std::int64_t>> file_storage;
        file_storage.emplace(std::piecewise_construct,
                             std::forward_as_tuple("original_file"),
                             std::forward_as_tuple(std::move(r_vector)));

        external_sort::algorithm("original_file",
                                buffer_size,
                                [&](const std::string& filename, std::size_t offset, char* buffer,
                                    std::size_t count) {
                                    // Read
                                    auto it = file_storage.find(filename);
                                    if (it == file_storage.end()) {
                                        throw std::logic_error("read_file: " + filename + " do not exist");
                                    }
                                    auto& file = it->second;
                                    std::memcpy(buffer, &file[offset], count * sizeof(std::int64_t));
                                },
                                [&](const std::string& filename, const char* buffer, std::size_t count) {
                                    // Write
                                    auto& output_file = file_storage[filename];
                                    std::size_t end = output_file.size();
                                    output_file.resize(output_file.size() + count);
                                    std::memcpy(&output_file[end], buffer, count * sizeof(std::int64_t));
                                },
                                [&](const std::string& filename) -> std::size_t {
                                    auto it = file_storage.find(filename);
                                    if (it == file_storage.end()) {
                                        throw std::logic_error("size_file: " + filename + " do not exist");
                                    }
                                    return it->second.size();
                                });

        auto& original_file = file_storage["original_file"];
        auto& output_file_1_stage_1 = file_storage["output_file_1.stage_1"];
        auto& output_file_2_stage_2 = file_storage["output_file_2.stage_1"];
        auto& final_file = file_storage["original_file.stage_final"];

        REQUIRE(output_file_1_stage_1.size() + output_file_2_stage_2.size() == original_file.size());
        REQUIRE(final_file.size() == original_file.size());
        REQUIRE(std::is_sorted(final_file.begin(), final_file.end()));
    };

    make_test_env(6333, 20);
    make_test_env(6333, 13);
    make_test_env(333333, 111);
    make_test_env(333333, 112);
    make_test_env(100, 10);
    make_test_env(100, 11);
    make_test_env(200, 10);
    make_test_env(200, 11);
    make_test_env(6000, 20);
    make_test_env(20, 20);
    make_test_env(20, 10);
    make_test_env(10, 10);
    make_test_env(9, 10);


}
